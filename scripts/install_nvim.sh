sudo apt -y install git curl python3-pip exuberant-ctags ack-grep
sudo pip3 install pynvim flake8 pylint isort jedi
sudo apt -y install nvim  
mkdir /tmp/install_nvim
cd /tmp/install_nvim
get_latest.py neovim neovim --filter appimage
sudo mv /usr/bin/nvim /usr/bin/nvim.old
sudo mv nvim.appimage /usr/bin/nvim
sudo chmod u+x /usr/bin/nvim

