#!/usr/bin/fish
pip3 install howdoi
pip3 install tldr
abbr -a "?" howdoi
mkdir -p ~/.local/bin
wget -O ~/.local/bin/cheatsheets https://raw.githubusercontent.com/cheat/cheat/master/scripts/git/cheatsheets
chmod +x ~/.local/bin/cheatsheets
mkdir /tmp/install_cheat
cd /tmp/install_cheat
get_latest.py cheat cheat --filter linux amd64
gunzip cheat-linux-amd64.gz
mv -f cheat-linux-amd64 ~/.local/bin/cheat
chmod u+x ~/.local/bin/cheat
