set option "--overwrite -R"
./linker.py -r ~/.config/vifm/colors/molokai.vifm $option
./linker.py -r ~/.config/vifm/vifmrc $option
./linker.py -r ~/.config/lazygit/config.yml $option
./linker.py -r ~/.config/lazydocker/config.yml $option
./linker.py -r ~/.config/themes.gitconfig $option
./linker.py -r ~/.config/nvim/custom.vim $option
./linker.py -r ~/.config/nvim/init.vim $option
./linker.py -r ~/.config/fish/config.fish $option
./linker.py -r ~/.config/fish/functions/fish_prompt.fish $option
./linker.py -r ~/.config/fish/fish_plugins $option
./linker.py -r ~/.config/kitty/session.kitty $option
./linker.py -r ~/.config/kitty/open-actions.conf $option
./linker.py -r ~/.config/kitty/kitty.conf $option

