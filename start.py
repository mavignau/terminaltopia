import subprocess

def run_cmd(cmd):
    cmd = "docker " + cmd
    out = subprocess.check_output(cmd.split(" "))
    return out.decode("utf8")

def get_docker_out():
    out = run_cmd("ps -a")
    split_line = lambda line: [x.strip() for x in line.split("  ") if x.strip()]
    lines = [x.strip() for x in out.split("\n") if x.strip()]
    titles = split_line(lines[0])
    sout = []
    for line in lines[1:]:
        values = split_line(line)
        sout.append(dict(zip(titles, values)))
    return sout

def remove_image():
    return run_cmd("image rm -f terminaltopia_image")

def build_image():
    return run_cmd("build -t terminaltopia_image docke/")

def run_image():
    pwd = "/".join(__file__.split("/")[:-1])
    return run_cmd(f'run --name terminaltopia_app -it --mount src={pwd},target=/test_container,type=bind --entrypoint /bin/bash terminaltopia_image')
