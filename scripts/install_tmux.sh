#!/usr/bin/fish
sudo apt install  -y tmux
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
pip install --upgrade tmuxp
mkdir -p ~/.config/tmuxp
abbr -a tmuxfreez "tmuxp freeze --force -y -f yaml --save-to ~/.config/tmuxp/session.yaml"
abbr -a tmuxload "tmuxp load ~/.config/tmuxp/session.yaml"
echo "You should type Ctrl-Space + I to fetch the plugin and source it."
