if status is-interactive
    # Commands to run in interactive sessions can go here
end
set -x EDITOR /usr/bin/nvim 
set -x CONFIG_REPO_PATH  ~/.local/config-repo/dotfiles
function hg --wraps rg; kitty +kitten hyperlinked_grep $argv; end
fish_add_path ~/.local/bin
fish_add_path ~/.bin
python3 ~/.local/config-repo/scripts/active_ip_forward.py &>/dev/null
alias shp=" TEMP=/tmp docker-compose -f docker/docker-compose.yml -f docker/docker-compose.app-endpoints.yml run -d --name shiphero-app-endpoints --service-ports app-endpoints bash"
alias noarmor="sudo aa-remove-unknown"
alias docker_stop="sudo aa-remove-unknown; docker container stop \$(docker container ls -a -q)"
alias wk="tmuxp load shp"
alias ls="lsd --group-dirs first"
alias vi="nvim"
alias wk2="tmuxp load shp2"
alias pomo="tomatoshell -f"
