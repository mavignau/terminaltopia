import subprocess
from pathlib import Path


def ip_forward_enabled():
    with open("/proc/sys/net/ipv4/ip_forward", "r") as fh:
        status = fh.read()
        return "1" in status


def get_line():
    """
    To add permissions to run w/o passwrd
    > sudo visudo
    > add bellow Add members of group sudo
    mavignau ALL=(ALL) NOPASSWD:  /usr/sbin/sysctl -w net.ipv4.ip_forward=1
    """
    return ["sudo", "sysctl", "-w", "net.ipv4.ip_forward=1"]


def main():
    if not ip_forward_enabled():
        process = subprocess.run(get_line(), check=True, stdout=subprocess.PIPE, universal_newlines=True)
        return process.stdout
    return "already net.ipv4.ip_forward = 1"


if __name__ == "__main__":
    print(main())
