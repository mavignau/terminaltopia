#!/usr/bin/fish
# first install file finder last version
get_latest.py sharkdp fd --filter "amd64" ".deb"
sudo dpkg --install fd*
rm fd*.deb
fisher install PatrickF1/fzf.fish

fzf_configure_bindings --history=\cr
fzf_configure_bindings --directory=\cf
fzf_configure_bindings --git_log=\cg
fzf_configure_bindings --git_status=\cs
fzf_configure_bindings --processes=\cp

echo "Try using "
echo "Ctrl+F (F for file)"
echo "Ctrl+G (L for git log)"
echo "Ctrl+S (S for git status)"
echo "Ctrl+R (R for reverse-i-search)"
echo "Ctrl+V (V for variable)"
echo "Ctrl+P (P for process)"
