mkdir /tmp/font
cd /tmp/font
wget https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/Terminus.zip
sudo apt -y install unzip
sudo mkdir -p /usr/local/share/fonts
unzip Terminus.zip
rm *Windows*
rm Terminus.zip
sudo cp * /usr/local/share/fonts


